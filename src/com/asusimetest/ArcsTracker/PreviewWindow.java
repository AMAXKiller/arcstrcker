package com.asusimetest.ArcsTracker;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.WindowManager.LayoutParams;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.PopupWindow.OnDismissListener;
import android.widget.TextView;

import java.io.File;
import java.lang.Runnable;
import java.lang.System;
import android.widget.ProgressBar;

public class PreviewWindow extends PopupWindow {
    public static final String TAG = PreviewWindow.class.getSimpleName();
    private final int MSG_SCRIPT_RUN_DONE = 0x1001;

    private TextView mPreviewTitle;
    private ImageButton mBtnRecord;
    private ImageButton mBtnPlay;
    private ImageButton mBtnDismiss;
    private ImageView mImgRec;
    private TextView mTvRecTime;
    private TouchCanvas mCanvasView;
    private String mRunningPath;
    private String mDataPath;
    private int mPosition;
    private Long mRecStrat;
    private Handler mRecHandler = new Handler();
    public static ProgressBar mPbPlay;

    static View mContentView;
    private Context mContext;

    public PreviewWindow(Context context, int layout_id) {
        mContext = context;
        mContentView = View.inflate(mContext, layout_id, null);
        mDataPath = mContext.getDir("Data", 0).getAbsolutePath() + "/test/";

        int h = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        int w = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        mContentView.measure(w, h);

        setContentView(mContentView);
        setWidth(LayoutParams.WRAP_CONTENT);
        setHeight(LayoutParams.WRAP_CONTENT);

        // workaround to listen the outside touch
        setBackgroundDrawable(new BitmapDrawable());
        setFocusable(true);
        setOutsideTouchable(false);
        setTouchInterceptor(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_OUTSIDE) {
                    dismiss();
                    return true;
                }
                else {
                    return false;
                }
            }
        });

        setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss() {
                mCanvasView.sIsRecording = false;
                mBtnRecord.setImageResource(R.drawable.ic_action_edit);
                mCanvasView.clearCanvas();
                mImgRec.setVisibility(View.GONE);
                mTvRecTime.setVisibility(View.GONE);
                mPbPlay.setVisibility(View.GONE);
                mRecHandler.removeCallbacks(mUpdateTimerRunnable);
                MainActivity.sGridViewCustomeAdapter.notifyDataSetChanged();
            }
        });

        setInputMethodMode(PopupWindow.INPUT_METHOD_NOT_NEEDED);
        mPreviewTitle = (TextView) mContentView.findViewById(R.id.preview_title);
        mBtnRecord = (ImageButton) mContentView.findViewById(R.id.btn_record);
        mBtnPlay = (ImageButton) mContentView.findViewById(R.id.btn_play);
        mBtnDismiss = (ImageButton) mContentView.findViewById(R.id.btn_dismiss);
        mImgRec = (ImageView) mContentView.findViewById(R.id.img_rec);
        mTvRecTime = (TextView) mContentView.findViewById(R.id.tv_rec_time);
        mCanvasView = (TouchCanvas) mContentView.findViewById(R.id.touchCanvas);
        mPbPlay = (ProgressBar) mContentView.findViewById(R.id.pb_play);

        mCanvasView.sIsRecording = false;
        mImgRec.setVisibility(View.GONE);
        mTvRecTime.setVisibility(View.GONE);
        mPbPlay.setVisibility(View.GONE);

        mBtnRecord.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mCanvasView.sIsRecording) {
                    mBtnRecord.setImageResource(R.drawable.ic_action_stop);
                    mBtnPlay.setEnabled(false);
                    mBtnDismiss.setEnabled(false);
                    mImgRec.setVisibility(View.VISIBLE);
                    mTvRecTime.setVisibility(View.VISIBLE);
                    mPbPlay.setVisibility(View.GONE);
                    mRecStrat = System.currentTimeMillis();
                    mRecHandler.removeCallbacks(mUpdateTimerRunnable);
                    mRecHandler.postDelayed(mUpdateTimerRunnable, 0);

                    mCanvasView.clearCanvas();
                    mCanvasView.startRecord();
                    mCanvasView.sIsRecording = true;
                } else {
                    mBtnRecord.setImageResource(R.drawable.ic_action_edit);
                    mBtnPlay.setEnabled(true);
                    mBtnDismiss.setEnabled(true);
                    mImgRec.setVisibility(View.GONE);
                    mTvRecTime.setVisibility(View.GONE);
                    mRecHandler.removeCallbacks(mUpdateTimerRunnable);

                    mCanvasView.saveRecord(mPreviewTitle.getText().toString(), new File(mDataPath + mPosition));
                    mCanvasView.sIsRecording = false;
                }
            }
        });

        mBtnPlay.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mCanvasView.sIsRecording) {
                    mCanvasView.sIsRecording = true;
                    mBtnRecord.setEnabled(false);
                    mBtnPlay.setEnabled(false);
                    mBtnDismiss.setEnabled(false);
                    mBtnPlay.setImageResource(R.drawable.ic_action_pause);
                    mPbPlay.setVisibility(View.VISIBLE);
                    mPbPlay.setProgress(0);

                    runScript(mDataPath + mPosition);
                }
            }
        });

        mBtnDismiss.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    public void setTitle(String text) {
        mPreviewTitle.setText(text);
        mPreviewTitle.setSelected(true);
    }

    public void setPosition(int p) {
        mPosition = p;
    }

    public int getPreviewViewHeight() {
        return mContentView.getMeasuredHeight();
    }

    public int getPreviewViewWidth() {
        return mContentView.getMeasuredWidth();
    }

    private void runScript(String path) {
        mCanvasView.clearCanvas();
        mRunningPath = path;
        new Thread(mPlayRunnable).start();
    }

    private Runnable mPlayRunnable = new Runnable() {
        public void run () {
            try {
                ScriptIntepreter interpreter = new ScriptIntepreter();
                interpreter.registerInvoker(new AutoMotionScriptInvoker());
                interpreter.loadFile(new File(mRunningPath));
                interpreter.setSpeed(1.0f);
                interpreter.run();
            }
            catch(Exception e) {
                Log.e("Exception when sendPointerSync", e.toString());
            }

            Message msg = new Message();
            msg.what = MSG_SCRIPT_RUN_DONE;
            mRunDoneHandler.sendMessage(msg);
        }
    };

    private Handler mRunDoneHandler = new Handler() {
        public void handleMessage(Message msg) {
            if (MSG_SCRIPT_RUN_DONE == msg.what) {
                mBtnRecord.setEnabled(true);
                mBtnDismiss.setEnabled(true);
                mBtnPlay.setEnabled(true);
                mBtnPlay.setImageResource(R.drawable.ic_action_play);
                mCanvasView.sIsRecording = false;
            }
        }
    };

    private Runnable mUpdateTimerRunnable = new Runnable() {
        public void run() {
            Long spentTime = System.currentTimeMillis() - mRecStrat;
            Long minius = (spentTime/1000)/60;
            Long seconds = (spentTime/1000) % 60;
            String min = (minius > 10) ? ""+minius : "0"+minius;
            String sec = (seconds > 10) ? ""+seconds : "0"+seconds;

            mTvRecTime.setText(min+":"+sec);
            mRecHandler.postDelayed(this, 1000);
        }
    };
}

