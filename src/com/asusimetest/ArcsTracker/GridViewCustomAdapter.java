package com.asusimetest.ArcsTracker;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.io.IOException;
import java.lang.Exception;
import java.util.ArrayList;

public class GridViewCustomAdapter extends ArrayAdapter {
    private Context mContext;
    public static ArrayList<String> mList;

    public GridViewCustomAdapter(Context context) {
        super(context, 0);
        mContext = context;

        mList = new ArrayList<String>();
        convertCodeAndGetText(MainActivity.sTestFilePath);
    }

    public int getCount() {
        return mList.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;

        if (row == null)  {
            LayoutInflater inflater = ((Activity)mContext).getLayoutInflater();
            row = inflater.inflate(R.layout.grid_row, parent, false);
        }

        File file = new File(mContext.getDir("Data", 0).getAbsolutePath() + "/test/" + (position+1));
        if (file.exists()) {
            row.setBackgroundColor(Color.LTGRAY);
        } else {
            row.setBackgroundColor(Color.TRANSPARENT);
        }

        TextView textViewTitle_num = (TextView) row.findViewById(R.id.textView_num);
        textViewTitle_num.setText("" + (position+1));

        TextView textViewTitle = (TextView) row.findViewById(R.id.textView);
        textViewTitle.setText(mList.get(position));

        return row;
    }

    private void convertCodeAndGetText(String str_filepath) {
        File file = new File(str_filepath);
        BufferedReader reader;
        String text = "";
        try {
            FileInputStream fis = new FileInputStream(file);
            BufferedInputStream in = new BufferedInputStream(fis);
            in.mark(4);
            byte[] first3bytes = new byte[3]; 
            in.read(first3bytes);
            in.reset();
            if (first3bytes[0] == (byte) 0xEF && first3bytes[1] == (byte) 0xBB
                    && first3bytes[2] == (byte) 0xBF) {
                reader = new BufferedReader(new InputStreamReader(in, "utf-8"));
            }
            else if (first3bytes[0] == (byte) 0xFF && first3bytes[1] == (byte) 0xFE) {
                reader = new BufferedReader(new InputStreamReader(in, "unicode"));
            }
            else if (first3bytes[0] == (byte) 0xFE && first3bytes[1] == (byte) 0xFF) {
                reader = new BufferedReader(new InputStreamReader(in, "utf-16be"));
            }
            else if (first3bytes[0] == (byte) 0xFF && first3bytes[1] == (byte) 0xFF) {
                reader = new BufferedReader(new InputStreamReader(in, "utf-16le"));
            }
            else {
                reader = new BufferedReader(new InputStreamReader(in, "Big5"));
            }

            String str = reader.readLine();
            str = reader.readLine(); // Skip first line
            while (str != null) {
                mList.add(str);
                str = reader.readLine();
            }
            reader.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
