package com.asusimetest.ArcsTracker;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.AssetManager;
import android.content.res.Configuration;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Random;

public class MainActivity extends Activity {
    // Constants
    private final String TAG = "ArcsTracker";
    private final int REQUEST_CODE_LOAD = 0x1001;
    private final String ASSET_FOLDER_NAME = "testfile";
    private final String CACHE_FOLDER_NAME = "test";
    private final String DEFAULT_TESTING_FILE = "IME_TEST_TW.txt";
    private final String DEFAULT_SAVING_SCRIPT = "MyScript.txt";
    private final String SP_NAME = "ArcsTracker";
    private final String SP_TESTING_FILE = "testingfile_path";
    private final String SP_SAVING_SCRIPT = "script_path";
    private final String SP_SAVING_SCRIPT_SUCESS = "script_sucess";
    // Views
    private PreviewWindow mPreviewWindow;
    private GridView mGridView;
    private EditText mEditTextScriptName;
    // Static Variables
    public static GridViewCustomAdapter sGridViewCustomeAdapter;
    public static String sTestFilePath;
    // Local Variables
    private String mSavingScriptPath;
    // Beginning Animation
    private KeywordsFlow keywordsFlow;
    public static final String[] keywords = {
        "腹有詩書氣自華", "一片春愁待酒澆", "明月幾時有", "料峭春風吹酒醒", "庭院深深深幾許",
        "自在飛花輕似夢", "歌盡桃花扇底風", "昨夜星辰昨夜風", "舞低楊柳樓心月", "滿城春色宮牆柳",
        "夕陽無限好", "但願人長久", "夜夜流光相皎潔", "千里共嬋娟", "夜夜金河復玉關",
        "似花還似非花", "若有人知春去處", "此地空餘黃鶴樓", "流水落花春去也", "也無風雨也無晴",
        "問君能有幾多愁", "衣帶漸寬終不悔", "流光容易把人拋", "紅杏枝頭春意鬧", "悲歡離合總無情",
        "回眸一笑百媚生", "桃花流水鮭魚肥", "一江春水向東流", "兩三星火是瓜州", "為賦新詞強說愁",
        "滿園春色關不住", "人生長恨水長東", "美人帳下猶歌舞", "天長地久有時盡", "古道西風瘦馬",
        "花底離愁三月雨", "樓頭殘夢五更鐘"
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        Log.d(TAG, "onCreate()");

        setRequestedOrientation(getScreenOrientation() == Configuration.ORIENTATION_PORTRAIT ?
            ActivityInfo.SCREEN_ORIENTATION_PORTRAIT : ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        copyAssets();

        // Set default testing file
        sTestFilePath = Environment.getExternalStorageDirectory().getPath() +
                "/" + ASSET_FOLDER_NAME + "/" + DEFAULT_TESTING_FILE;
        // Set default saving script
        mSavingScriptPath = Environment.getExternalStorageDirectory().getPath() +
                "/" + DEFAULT_SAVING_SCRIPT;

        // Load shared preferences
        SharedPreferences sp = getSharedPreferences(SP_NAME, 0);
        sTestFilePath = sp.getString(SP_TESTING_FILE, sTestFilePath);
        mSavingScriptPath = sp.getString(SP_SAVING_SCRIPT, mSavingScriptPath);

        if (!(new File(sTestFilePath).exists())) {
            sTestFilePath = Environment.getExternalStorageDirectory().getPath() +
                    "/" + ASSET_FOLDER_NAME + "/" + DEFAULT_TESTING_FILE;
        }

        Log.d(TAG, "sTestFilePath = " + sTestFilePath);
        Log.d(TAG, "mSavingScriptPath = " + mSavingScriptPath);

        removeCaches();
        findViews();
        loadScript();
    }

    /*@Override
    public void onResume() {
        super.onResume();
    }*/

    @Override
    public void onPause() {
        super.onPause();

        Log.d(TAG, "onPause()");
        Log.d(TAG, "mSavingScriptPath = " + mSavingScriptPath);
        Log.d(TAG, "sTestFilePath = " + sTestFilePath);

        saveScript();

        SharedPreferences sp = getSharedPreferences(SP_NAME, 0);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(SP_TESTING_FILE, sTestFilePath);
        editor.putString(SP_SAVING_SCRIPT, mSavingScriptPath);
        editor.commit();
    }

    private void findViews() {
        mGridView = (GridView) findViewById(R.id.gridView);
        mGridView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View view, int position, long arg3) {
                if (mPreviewWindow == null) {
                    mPreviewWindow = new PreviewWindow(getApplicationContext(),
                        R.layout.preview_window);
                }
                mPreviewWindow.setTitle(GridViewCustomAdapter.mList.get(position));
                mPreviewWindow.setPosition(position+1);

                int ScreenWidth = getResources().getDisplayMetrics().widthPixels;
                int ScreenHeight = getResources().getDisplayMetrics().heightPixels;

                int x = ScreenWidth/2 - mPreviewWindow.getPreviewViewWidth()/2;
                int y = ScreenHeight/2 - mPreviewWindow.getPreviewViewHeight()/2;

                mPreviewWindow.showAtLocation(view, Gravity.NO_GRAVITY, x, y);
            }
        });

        keywordsFlow = (KeywordsFlow) findViewById(R.id.keywordsFlow);
        //keywordsFlow.setDuration(800l);
        //feedKeywordsFlow(keywordsFlow, keywords);
        //keywordsFlow.go2Show(KeywordsFlow.ANIMATION_IN);

        sGridViewCustomeAdapter = new GridViewCustomAdapter(this);
        mGridView.setAdapter(sGridViewCustomeAdapter);
        keywordsFlow.setVisibility(View.GONE);
        mGridView.setVisibility(View.VISIBLE);
    }

    private static void feedKeywordsFlow(KeywordsFlow keywordsFlow, String[] arr) {  
        Random random = new Random();  
        for (int i = 0; i < KeywordsFlow.MAX; i++) {  
            int ran = random.nextInt(arr.length);  
            String tmp = arr[ran];  
            keywordsFlow.feedKeyword(tmp);  
        }  
    }  

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_load:
                Intent intent_load = new Intent(Intent.ACTION_GET_CONTENT);
                intent_load.setType("text/plain");
                intent_load.addCategory(Intent.CATEGORY_OPENABLE);
                startActivityForResult(
                        Intent.createChooser(intent_load, getString(R.string.select_file_title)),
                        REQUEST_CODE_LOAD);
                return true;
            case R.id.action_save:
                showSavingDialog();
                return true;
            case R.id.action_email:
                sendEmail();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (null == data) return;
        switch (requestCode) {
            case REQUEST_CODE_LOAD:
                Uri uri = data.getData();
                String filePath = getUriPath(uri);

                removeCaches();

                if (IsIMETestingFile(filePath)) {
                    sTestFilePath = filePath;
                    mSavingScriptPath = Environment.getExternalStorageDirectory().getPath() +
                        "/" + (new File(filePath).getName()) + "_" + DEFAULT_SAVING_SCRIPT;
                    sGridViewCustomeAdapter = new GridViewCustomAdapter(this);
                    mGridView.setAdapter(sGridViewCustomeAdapter);

                    keywordsFlow.setVisibility(View.GONE);
                    mGridView.setVisibility(View.VISIBLE);
                }
                else if (IsIMESavingScript(filePath)) {
                    mSavingScriptPath = filePath;
                    LoadIMESavingScript(mSavingScriptPath);
                    sGridViewCustomeAdapter.notifyDataSetChanged();
                }
                else {
                    Toast.makeText(getApplicationContext(),
                            getString(R.string.load_file_fail),
                            Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    private  boolean removeDirectory(File directory) {
        if (directory == null) return false;
        if (!directory.exists()) return true;
        if (!directory.isDirectory()) return false;

        String[] list = directory.list();

        if (list != null) {
            for (int i = 0; i < list.length; i++) {
                File entry = new File(directory, list[i]);

                if (entry.isDirectory()) {
                    if (!removeDirectory(entry)) return false;
                }
                else {
                    if (!entry.delete()) return false;
                }
            }
        }

        return directory.delete();
    }

    public String getUriPath(Uri uri) {
        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = { "_data" };
            Cursor cursor = null;

            try {
                cursor = getContentResolver().query(uri, projection, null, null, null);
                int column_index = cursor.getColumnIndexOrThrow("_data");
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            }
            catch (Exception e) {
            }
        }
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    private void showSavingDialog() {
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.saving_script, null);

        TextView testItemNum = (TextView) view.findViewById(R.id.testing_items_num);
        TextView compItemNum = (TextView) view.findViewById(R.id.compeleted_items_num);
        mEditTextScriptName = (EditText) view.findViewById(R.id.edt_saving_script);

        int file_num = getFileNum(new File(this.getDir("Data", 0).getAbsolutePath() + "/" + CACHE_FOLDER_NAME));

        testItemNum.setText(" " + sGridViewCustomeAdapter.getCount());
        compItemNum.setText(" " + file_num);
        mEditTextScriptName.setText(DEFAULT_SAVING_SCRIPT);

        AlertDialog.Builder alert = new AlertDialog.Builder(this, android.R.style.Theme_DeviceDefault_Light_Dialog);
        alert.setTitle(getResources().getString(R.string.saving_dialog_title));
        alert.setView(view);
        alert.setCancelable(false);
        alert.setPositiveButton(getResources().getString(R.string.done),
            new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    mSavingScriptPath = Environment.getExternalStorageDirectory().getPath()
                        + "/" + mEditTextScriptName.getText().toString();
                    saveScript();
                }
            });
        alert.setNegativeButton(getResources().getString(R.string.cancel),
            new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

        AlertDialog savingDialog = alert.create();
        savingDialog.show();
    }

    private void sendEmail() {
        saveScript();

        File file = new File(mSavingScriptPath);
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_EMAIL, new String[] {"pmdswrd@gmail.com", "angela_liao@asus.com"});
        intent.putExtra(Intent.EXTRA_SUBJECT, "[ArcTracker] Report My Arcs Script");
        intent.putExtra(Intent.EXTRA_TEXT, "Hello Asus! My name is : ");

        if (!file.exists() || !file.canRead()) {
            Toast.makeText(getApplicationContext(), "Attachment Error", Toast.LENGTH_SHORT).show();
            return;
        }

        Uri uri = Uri.fromFile(file);
        intent.putExtra(Intent.EXTRA_STREAM, uri);

        startActivity(Intent.createChooser(intent, "Send email..."));
    }

    private int getFileNum(File f) {
        File flist[] = f.listFiles();
        return flist.length;
    }

    private boolean IsIMETestingFile(String path) {
        Log.d("Bohsuan", "IsIMETestingFile? " + path);

        File file = new File(path);
        try {
            FileInputStream fis = new FileInputStream(file);
            BufferedInputStream in = new BufferedInputStream(fis);
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            String str = reader.readLine();

            Log.d("Bohsuan", "first line = " + str);

            if (str.contains("# ASUS IME TESTING FILE #")) {
                return true;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return false;
    }

    private boolean IsIMESavingScript(String path) {
        File file = new File(path);
        try {
            FileInputStream fis = new FileInputStream(file);
            BufferedInputStream in = new BufferedInputStream(fis);
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            String str = reader.readLine();

            if (str.contains("# ASUS IME TESTING SCRIPT #")) {
                int ScreenWidth = getResources().getDisplayMetrics().widthPixels;
                int ScreenHeight = getResources().getDisplayMetrics().heightPixels;

                str = reader.readLine();
                if (!str.equals("# ScreenWidth=" + ScreenWidth))
                    return false;
                str = reader.readLine();
                if (!str.equals("# ScreenHeight=" + ScreenHeight))
                    return false;

                return true;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return false;
    }

    private void LoadIMESavingScript(String path) {
        File file = new File(path);
        try {
            FileInputStream fis = new FileInputStream(file);
            BufferedInputStream in = new BufferedInputStream(fis);
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            String str = reader.readLine();

            while (str != null) {
                if (str.startsWith("# start")) {
                    String test_case = str.substring(str.indexOf('=') + 1, str.length());
                    File file_test = new File(getApplicationContext().getDir("Data", 0).getAbsolutePath()
                            + "/" + CACHE_FOLDER_NAME + "/" + (GridViewCustomAdapter.mList.indexOf(test_case) + 1));
                    file_test.delete();

                    FileWriter fw = new FileWriter(file_test);
                    while (!str.startsWith("# end")) {
                        fw.write(str);
                        fw.write("\r\n");
                        str = reader.readLine();
                    }
                    fw.write(str);
                    fw.flush();
                    fw.close();
                }
                str = reader.readLine();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public int getScreenOrientation()
    {
        Display getOrient = getWindowManager().getDefaultDisplay();
        int orientation = Configuration.ORIENTATION_UNDEFINED;
        if(getOrient.getWidth()==getOrient.getHeight()){
            orientation = Configuration.ORIENTATION_SQUARE;
        } else{ 
            if(getOrient.getWidth() < getOrient.getHeight()){
                orientation = Configuration.ORIENTATION_PORTRAIT;
            }else { 
                orientation = Configuration.ORIENTATION_LANDSCAPE;
            }
        }
        return orientation;
    }

    private void copyAssets() {
        AssetManager assetManager = getAssets();
        String[] files = null;

        try {
            files = assetManager.list(ASSET_FOLDER_NAME);
        }
        catch (IOException e) {
            Log.e(TAG, "Failed to get asset file list.", e);
        }

        for (String filename : files) {
            Log.d(TAG, "copyAssets: " + filename);
            InputStream in = null;
            OutputStream out = null;

            try {
                File outFile = new File(Environment.getExternalStorageDirectory().getPath() +
                        "/" + ASSET_FOLDER_NAME + "/" + filename);
                if (!outFile.getParentFile().exists()) {
                    outFile.getParentFile().mkdirs();
                }

                in = assetManager.open(ASSET_FOLDER_NAME + "/" + filename);
                out = new FileOutputStream(outFile);
                copyFile(in, out);
                out.flush();

                in.close();
                in = null;
                out.close();
                out = null;
            }
            catch (IOException e) {
                Log.e(TAG, "Failed to copy asset file: " + filename, e);
            }       
        }
    }

    private void copyFile(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        int read;
        while((read = in.read(buffer)) != -1){
            out.write(buffer, 0, read);
        }
    }

    private void saveScript() {
        SharedPreferences sp = getSharedPreferences(SP_NAME, 0);
        SharedPreferences.Editor editor = sp.edit();
        editor.putBoolean(SP_SAVING_SCRIPT_SUCESS, false);
        editor.commit();

        File[] flist_temp = new File(getApplicationContext().getDir("Data", 0).getAbsolutePath()
                + "/" + CACHE_FOLDER_NAME).listFiles();

        if (flist_temp  == null) {
            Toast.makeText(getApplicationContext(),
                    getString(R.string.save_file_fail),
                    Toast.LENGTH_LONG).show();

            editor.putBoolean(SP_SAVING_SCRIPT_SUCESS, true);
            editor.commit();

            return;
        }


        //Log.d(TAG, Arrays.toString(flist_temp));

        ArrayList<File> arrayList = new ArrayList<File>(Arrays.asList(flist_temp));
        Collections.sort(arrayList, new Comparator<File>() {
            @Override
            public int compare(File f1, File f2) {
                int f1_num = Integer.valueOf(f1.getName());
                int f2_num = Integer.valueOf(f2.getName());
                int ret = 0;

                if (f1_num < f2_num) ret = -1;
                if (f1_num == f2_num) ret = 0;
                if (f1_num > f2_num) ret = 1;

                return ret;
            }
        });

        File[] flist = (File[])arrayList.toArray(new File[0]);

        //Log.d(TAG, Arrays.toString(flist));

        if (flist.length <= 0) {
            Toast.makeText(getApplicationContext(),
                    getString(R.string.save_file_fail),
                    Toast.LENGTH_LONG).show();

            editor.putBoolean(SP_SAVING_SCRIPT_SUCESS, true);
            editor.commit();

            return;
        }

        File saving_file = new File(mSavingScriptPath);

        try {
            FileWriter fw = new FileWriter(saving_file);

            if (mPreviewWindow == null) {
                mPreviewWindow = new PreviewWindow(getApplicationContext(),
                        R.layout.preview_window);
            }

            int ScreenWidth = getResources().getDisplayMetrics().widthPixels;
            int ScreenHeight = getResources().getDisplayMetrics().heightPixels;
            int PreviewViewWidth = mPreviewWindow.getPreviewViewWidth();
            int PreviewViewHeight = mPreviewWindow.getPreviewViewHeight();
            int StartX = ScreenWidth/2 - PreviewViewWidth/2;
            int StartY = ScreenHeight/2 - PreviewViewHeight/2;

            fw.write("# ASUS IME TESTING SCRIPT #\r\n");
            fw.write("# ScreenWidth=" + ScreenWidth + "\r\n");
            fw.write("# ScreenHeight=" + ScreenHeight + "\r\n");
            fw.write("# PreviewViewWidth=" + PreviewViewWidth + "\r\n");
            fw.write("# PreviewViewHeight=" + PreviewViewHeight + "\r\n");
            fw.write("# StartX=" + StartX + "\r\n");
            fw.write("# StartY=" + StartY + "\r\n");

            fw.write("sleep 1000\r\n\r\n");
            for (int i = 0; i < flist.length; i++) {
                try {
                    FileInputStream fis = new FileInputStream(flist[i]);
                    BufferedReader br = new BufferedReader(new InputStreamReader(fis));
                    for (String line = br.readLine(); null != line; line = br.readLine()) {
                        fw.write(line);
                        fw.write("\r\n");
                    }
                    br.close();
                    fis.close();
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
                fw.write("\r\n");
                fw.write("sleep 1000\r\n\r\n");
            }

            fw.flush();
            fw.close();

            MediaScannerConnection.scanFile(getApplicationContext(),
                    new String[] { saving_file.getAbsolutePath() }, null, null);

            Toast.makeText(getApplicationContext(),
                    getString(R.string.save_file_to) + " " + saving_file.getAbsolutePath(),
                    Toast.LENGTH_LONG).show();

            editor.putBoolean(SP_SAVING_SCRIPT_SUCESS, true);
            editor.commit();
        }
        catch (Exception e) {
            e.printStackTrace();
        }

    }

    void loadScript() {
        if (IsIMESavingScript(mSavingScriptPath)) {
            LoadIMESavingScript(mSavingScriptPath);
            sGridViewCustomeAdapter.notifyDataSetChanged();
        }
        else {
            Toast.makeText(getApplicationContext(),
                    getString(R.string.load_file_fail),
                    Toast.LENGTH_LONG).show();
        }
    }

    void removeCaches() {
        SharedPreferences sp = getSharedPreferences(SP_NAME, 0);
        boolean ret = sp.getBoolean(SP_SAVING_SCRIPT_SUCESS, false);

        if (!ret) {
            Log.d(TAG, "Last time Saving Script Fail, save again");
            saveScript();
            removeCaches();
            return;
        }

        File file = new File(this.getDir("Data", 0).getAbsolutePath() + "/" + CACHE_FOLDER_NAME);
        removeDirectory(file);
        file.mkdirs();
    }
}
