# To keep arcs for handwriting characters:
1. Load test file.
2. Record arcs of hand writing.
3. Re-play arcs in display screen.
4. Save arcs as a script file.


Test file format like:

```
#!python

# ASUS IME TESTING FILE #
的
一
是
有
在
人
.
.
.
```

