LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

LOCAL_AAPT_INCLUDE_ALL_RESOURCES := true

LOCAL_MODULE_TAGS := optional
LOCAL_CERTIFICATE := AMAX
LOCAL_SRC_FILES := $(call all-subdir-java-files)

LOCAL_PACKAGE_NAME := ArcsTracker

include $(BUILD_PACKAGE)
